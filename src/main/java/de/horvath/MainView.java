package de.horvath;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class MainView {

	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}